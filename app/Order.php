<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\OrderDetails;

class Order extends Model
{ 
    protected $primaryKey = 'orderNumber';

    //products that belong to the order
    public function products()
    {
        return $this->belongsToMany('App\Product',OrderDetails::class,'orderNumber', 'productCode')
                    ->withPivot('quantityOrdered', 'priceEach');
    }

     /**
     * Get the customer that owns the order.
     */
    public function customers()
    {
        return $this->belongsTo('App\Customer','customerNumber');
    }
}
