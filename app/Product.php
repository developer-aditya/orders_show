<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $primaryKey = 'productCode';
    //the order that belongs to the product
    public function orders()
    {
      return $this->belongsToMany('App\Order');
    }
}
