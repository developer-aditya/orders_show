<?php

namespace App\Http\Controllers;

use App\Order;
use App\Customer;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    public function fetchOrderData($id)
    {
       $order = Order::findOrFail($id);
       $products = $order->products()->get();
       $total = 0.00;
       $pArray = [];
       $cArray = [];
       foreach($products as $product)
       {
           $pro['product'] = $product->productName;
           $pro['product_line'] = $product->productLine;
           $pro['unit_price'] = $product->pivot->priceEach;
           $pro['qty'] = $product->pivot->quantityOrdered;
           $pro['line_total'] =  $product->pivot->priceEach * $product->pivot->quantityOrdered;
           $total = $total +  $pro['line_total'] ;
           $pArray[]= $pro ; 
       }
       $customers = $order->customers()->get();
       foreach($customers as $customer)
       {
         $cus["first_name"] = $customer->contactFirstName;
         $cus["last_name"] = $customer->contactLastName;
         $cus["phone"] = $customer->phone;
         $cus["country_code"] = $customer->country;
         $cArray[] = $cus;
       }
       $res = array(
        "order_id" => $id,
        "order_date" => $order->orderDate ,
        "status"=> $order->status,
        "order_details" => $pArray,
        "bill_amount" => number_format($total,2),
        "customer"  => $cArray,
       );
      //get the json result
       return response()->json($res);
    }
}
